unit ViewErrorFormU;

{$mode objfpc}{$H+}

interface

uses
 Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

 { TViewErrorForm }

 TViewErrorForm = class(TForm)
  txtError: TMemo;
 private
  { private declarations }
 public
  { public declarations }
 end;

var
 ViewErrorForm: TViewErrorForm;

implementation

{$R *.lfm}

end.

