unit MainFormU;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, EditBtn,
  StdCtrls, Buttons, ComCtrls, Menus, strutils, FileTextSearch, fgl,
  FileAssociationDetails, types,LCLIntf,LCLType, PopupNotifier;

type




  { TFoundTextItem }

  TFoundTextItem = class
  public
    TextFound:string;
    Filename:string;
    AssociatedImage:TBitmap;
    constructor Create;
    destructor Destroy;
  end;

  { TFoundTextItems }

  TFoundTextItems = class (specialize TFPGObjectList<TFoundTextItem>)
    function FindFoundText(Filename:string):string;
  end;

  { TMainForm }

  TMainForm = class(TForm)
   ApplicationProperties1: TApplicationProperties;
    btnSearch: TBitBtn;
    lblFileCount: TLabel;
    lblSearchProgress: TLabel;
    lbLResult: TLabel;
    lstResults: TListBox;
    itmOpenFile: TMenuItem;
    MainMenu1: TMainMenu;
    mitAbout: TMenuItem;
    mitHelp: TMenuItem;
    mitViewErrors: TMenuItem;
    mitTools: TMenuItem;
    mitExit: TMenuItem;
    mitFile: TMenuItem;
    mitPreviewText: TMenuItem;
    mitSep1: TMenuItem;
    mitOpenFolder: TMenuItem;
    PopupMenu1: TPopupMenu;
    PopupNotifier1: TPopupNotifier;
    ProgressBar1: TProgressBar;
    txtSearchWord: TEdit;
    lblSearchWord: TLabel;
    txtRootFolder: TDirectoryEdit;
    Label1: TLabel;
    procedure ApplicationProperties1DropFiles(Sender: TObject;
     const FileNames: array of String);
    procedure btnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure itmOpenFileClick(Sender: TObject);
    procedure lblAboutClick(Sender: TObject);
    procedure lstResultsDrawItem(Control: TWinControl; Index: Integer;
     ARect: TRect; State: TOwnerDrawState);
    procedure lstResultsShowHint(Sender: TObject; HintInfo: PHintInfo);
    procedure mitAboutClick(Sender: TObject);
    procedure mitExitClick(Sender: TObject);
    procedure mitFileClick(Sender: TObject);
    procedure mitOpenFolderClick(Sender: TObject);
    procedure mitPreviewTextClick(Sender: TObject);
    procedure mitSep1Click(Sender: TObject);
    procedure mitViewErrorsClick(Sender: TObject);
    procedure txtResultChange(Sender: TObject);
  private
    searchThread:TFileSearchThread;
    foundItems:TFoundTextItems;
    imgGradient:TBitmap;
    lstErrors:TStringList;
    procedure OnThreadTermiate(sender:TObject);
    procedure OnCurrentFileScan(Sender:TObject; const CurrentFilename:string);
    procedure OnTextFound(Sender:TObject; const TextFound:String; const Filename:string);
    procedure OnFileCount(Sender:TObject; const Count:Integer);
    procedure OnError(Sender:TObject; const ErrorMessage:string; const Filename:string);
  public
    { public declarations }
  end;





var
  MainForm: TMainForm;

implementation

uses ViewErrorFormU;


{$R *.lfm}

{ TFoundTextItem }

constructor TFoundTextItem.Create;
begin
 AssociatedImage := TBitmap.Create;
end;

destructor TFoundTextItem.Destroy;
begin
 if AssociatedImage<>nil then
 AssociatedImage.Free;
end;

{ TFoundTextItems }

function TFoundTextItems.FindFoundText(Filename: string): string;
var
 I: Integer;
begin
 for I:=0 to Count-1 do
 begin
   if Items[I].Filename = Filename then
   begin
    Result := Items[I].TextFound;
    Break;
   end;
 end;
end;

{ TMainForm }

procedure TMainForm.btnSearchClick(Sender: TObject);
begin
  if btnSearch.Caption='Search' then
  begin
  searchThread := TFileSearchThread.Create(txtRootFolder.Text,txtSearchWord.Text);
  searchThread.OnTerminate:=@OnThreadTermiate;
  searchThread.OnCurrentFileScan:=@OnCurrentFileScan;
  searchThread.OnTextFound:=@OnTextFound;
  searchThread.OnFileCount:=@OnFileCount;
  searchThread.OnError:=@OnError;
  searchThread.FreeOnTerminate:=True;

  foundItems.Clear;
  lstResults.Clear;
  lstErrors.Clear;

  ProgressBar1.Style:=pbstMarquee;
  btnSearch.Caption:='Cancel';



  searchThread.Start;
  end else
  begin
    if searchThread <> nil then
    searchThread.Cancel;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
 foundItems:=TFoundTextItems.Create(True);
 lstErrors:=TStringList.Create;
  imgGradient := TBitmap.Create;
  imgGradient.Width := 10;
  imgGradient.Height := 20;
  imgGradient.Canvas.GradientFill(Rect(0, 0, 20, 20), clSkyBlue, clCream, gdVertical);
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  lstErrors.Free;
 foundItems.Free;
 imgGradient.Free;
end;

procedure TMainForm.FormShow(Sender: TObject);
begin
 txtRootFolder.Text:=ParamStr(1);
 txtSearchWord.Text:= ParamStr(2);
end;

procedure TMainForm.itmOpenFileClick(Sender: TObject);
begin
  if lstResults.ItemIndex>-1 then
 OpenDocument(foundItems[lstResults.ItemIndex].Filename);
end;

procedure TMainForm.lblAboutClick(Sender: TObject);
begin

end;

procedure TMainForm.lstResultsDrawItem(Control: TWinControl; Index: Integer;
 ARect: TRect; State: TOwnerDrawState);
var
 c:TCanvas;
begin
  if (Control is TListBox) then
   c := (Control as TListBox).Canvas
  else
    exit;

  if Odd(Index) then
    c.Brush.Color := clWhite
  else
    c.Brush.Color := $00F9F9F9;

  c.FillRect(ARect);

  if odSelected in State then
  begin
    c.Font.Color:=clWindowText;
    c.StretchDraw(ARect, imgGradient);
  end;

  c.Draw(ARect.Left,ARect.Top,foundItems[Index].AssociatedImage);

  c.Brush.Style:=bsClear;
  c.Font.Size:=10;
  c.Font.Style:=[fsBold];
  c.TextOut(ARect.Left+48,ARect.Top,ExtractFileName(foundItems[Index].Filename));


  c.Font.Size:=8;
  c.Font.Style:=c.Font.Style-[fsBold];
  c.TextOut(ARect.Left+48,ARect.Top+20,ExtractFilePath(foundItems[Index].Filename));




end;

procedure TMainForm.lstResultsShowHint(Sender: TObject; HintInfo: PHintInfo);
begin
end;

procedure TMainForm.mitAboutClick(Sender: TObject);
begin
  ShowMessage
 (
  'LazFileTextSearch'+LineEnding+
  ' '+LineEnding+
  'Flakron Shkodra © 2014 '
 );
end;

procedure TMainForm.mitExitClick(Sender: TObject);
begin
 Application.Terminate;
end;

procedure TMainForm.mitFileClick(Sender: TObject);
begin

end;

procedure TMainForm.mitOpenFolderClick(Sender: TObject);
begin
 if lstResults.ItemIndex>-1 then
 OpenURL(ExtractFilePath(foundItems[lstResults.ItemIndex].Filename));
end;

procedure TMainForm.mitPreviewTextClick(Sender: TObject);
begin
 if lstResults.ItemIndex>-1 then
 begin
  PopupNotifier1.Text:=foundItems[lstResults.ItemIndex].TextFound;
  PopupNotifier1.Icon.Assign(foundItems[lstResults.ItemIndex].AssociatedImage);
  PopupNotifier1.Title:= ExtractFilename(foundItems[lstResults.ItemIndex].Filename);
  PopupNotifier1.ShowAtPos(Mouse.CursorPos.x,Mouse.CursorPos.y);
 end;
end;

procedure TMainForm.mitSep1Click(Sender: TObject);
begin

end;

procedure TMainForm.mitViewErrorsClick(Sender: TObject);
begin
 ViewErrorForm.txtError.Text:=lstErrors.Text;
 ViewErrorForm.ShowModal;
end;

procedure TMainForm.txtResultChange(Sender: TObject);
begin
 lbLResult.Caption:='Search word appears in ['+IntToStr(lstResults.Count)+']';
end;

procedure TMainForm.ApplicationProperties1DropFiles(Sender: TObject;
 const FileNames: array of String);
begin
  if Length(FileNames)>0 then
 txtRootFolder.Text:=FileNames[0];
end;

procedure TMainForm.OnThreadTermiate(sender: TObject);
begin
 ProgressBar1.Style:=pbstNormal;
 btnSearch.Caption:='Search';
 lblSearchProgress.Caption:='';
 lblFileCount.Caption:='';
end;

procedure TMainForm.OnCurrentFileScan(Sender: TObject;
 const CurrentFilename: string);
begin
 lblSearchProgress.Caption:='Scanning file: '+ExtractFileName(CurrentFilename);
end;

procedure TMainForm.OnTextFound(Sender: TObject; const TextFound: String;
 const Filename: string);
var
  found:TFoundTextItem;
  fa:TFileAssociationDetails;
begin

  found := TFoundTextItem.Create;
  found.Filename:=Filename;
  found.TextFound:=TextFound;

  fa := TFileAssociationDetails.Create;
  try
    fa.AddFile(Filename);
    fa.GetFileIconsAndDescriptions;
    fa.Images.GetBitmap(0,found.AssociatedImage);
  finally
    fa.Free;
  end;
  foundItems.Add(found);
  lstResults.Items.Add(Filename);
end;

procedure TMainForm.OnFileCount(Sender: TObject; const Count: Integer);
begin
   lblFileCount.Caption:='Searching in '+IntToStr(Count)+' files';
end;

procedure TMainForm.OnError(Sender: TObject; const ErrorMessage: string;
 const Filename: string);
begin
  lstErrors.Add('Error on file ['+Filename+'] : '+ ErrorMessage);
end;


end.
