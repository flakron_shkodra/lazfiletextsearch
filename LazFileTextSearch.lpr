program LazFileTextSearch;

{$mode objfpc}{$H+}

uses
 {$IFDEF UNIX}{$IFDEF UseCThreads}
 cthreads,
 {$ENDIF}{$ENDIF}
 Interfaces, // this includes the LCL widgetset
 Forms, MainFormU, FileTextSearch, FileAssociationDetails, ViewErrorFormU;

{$R *.res}

begin
 RequireDerivedFormResource := True;
 Application.Initialize;
 Application.CreateForm(TMainForm, MainForm);
 Application.CreateForm(TViewErrorForm, ViewErrorForm);
 Application.Run;
end.

