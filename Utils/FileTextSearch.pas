unit FileTextSearch;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, FileUtil, strutils, comobj, ActiveX;

{ TFileSearchThread }

const

  OfficeServerName = 'Word.Application';
  PdfServerName = 'AcroExch.pdDoc';
  PdfHighlightServerName='AcroExch.HiliteList';

type

  TSearchNotifyEvent = procedure(Sender: TObject; const OutputValue: integer) of object;
  TSearchNotifyNameEvent = procedure(Sender: TObject;
    const CurrentFilename: string) of object;
  TSearchNotifyTextFound = procedure (Sender:TObject; const TextFound:string; const Filename:string) of object;
  TSearchNotifyError = procedure (Sender:TObject; const ErroMessage:string; const Filename:string) of object;


  TFileSearchThread = class(TThread)
  private
    FOnCurrentFileScan: TSearchNotifyNameEvent;
    FOnError: TSearchNotifyError;
    FOnFileCount: TSearchNotifyEvent;
    FOnSearchProgress: TSearchNotifyEvent;
    FOnTextFound: TSearchNotifyTextFound;
    FShouldStop: boolean;
    FSearchFolder: string;
    FSearchKeyWord: string;
    procedure SetOnCurrentFileScan(AValue: TSearchNotifyNameEvent);
    procedure SetOnError(AValue: TSearchNotifyError);
    procedure SetOnFileCount(AValue: TSearchNotifyEvent);
    procedure SetOnSearchProgress(AValue: TSearchNotifyEvent);
    function FileIsDoc(Filename: string): boolean;
    function FileIsPdf(Filename:string):Boolean;
    procedure SetOnTextFound(AValue: TSearchNotifyTextFound);
  protected
    procedure Execute; override;
  public
    constructor Create(SearchFolder, SearchKeyWord: string);
    procedure Cancel;
  published
    property OnFileCount: TSearchNotifyEvent read FOnFileCount write SetOnFileCount;
    property OnSearchProgress: TSearchNotifyEvent
      read FOnSearchProgress write SetOnSearchProgress;
    property OnCurrentFileScan: TSearchNotifyNameEvent
      read FOnCurrentFileScan write SetOnCurrentFileScan;
    property OnTextFound:TSearchNotifyTextFound read FOnTextFound write SetOnTextFound;
    property OnError:TSearchNotifyError read FOnError write SetOnError;
  end;



implementation

{ TFileSearchThread }

procedure TFileSearchThread.SetOnFileCount(AValue: TSearchNotifyEvent);
begin
  if FOnFileCount = AValue then
    Exit;
  FOnFileCount := AValue;
end;

procedure TFileSearchThread.SetOnCurrentFileScan(AValue: TSearchNotifyNameEvent);
begin
  if FOnCurrentFileScan = AValue then
    Exit;
  FOnCurrentFileScan := AValue;
end;

procedure TFileSearchThread.SetOnError(AValue: TSearchNotifyError);
begin
 if FOnError=AValue then Exit;
 FOnError:=AValue;
end;

procedure TFileSearchThread.SetOnSearchProgress(AValue: TSearchNotifyEvent);
begin
  if FOnSearchProgress = AValue then
    Exit;
  FOnSearchProgress := AValue;
end;

function TFileSearchThread.FileIsDoc(Filename: string): boolean;
begin
  Result := (LowerCase(ExtractFileExt(Filename)) = '.doc') or
    (LowerCase(ExtractFileExt(Filename)) = '.docx');
end;

function TFileSearchThread.FileIsPdf(Filename: string): Boolean;
begin
    Result := (LowerCase(ExtractFileExt(Filename)) = '.pdf');
end;

procedure TFileSearchThread.SetOnTextFound(AValue: TSearchNotifyTextFound);
begin
 if FOnTextFound=AValue then Exit;
 FOnTextFound:=AValue;
end;

procedure TFileSearchThread.Execute;
var
  lstAllFiles: TStringList;
  lstContent: TStringList;
  I,J: integer;
  oleServer: variant;
  oleDoc: variant;
  wFilename: WideString;
  wText: string;
  pdfDoc,PDPage,PDHili,PDTextS: Variant;
  pCount:Integer;
  b:Boolean;
  k: Integer;
begin
  lstAllFiles := FindAllFiles(FSearchFolder, '*', True);



  if FOnFileCount <> nil then
  begin
    FOnFileCount(Self, lstAllFiles.Count);
  end;

  lstContent := TStringList.Create;
  try

    CoInitialize(nil);

    for i := 0 to lstAllFiles.Count - 1 do
    begin

      //notify for current file being scan
      if FOnCurrentFileScan <> nil then
      begin
        FOnCurrentFileScan(Self, lstAllFiles[I]);
      end;

      if FileIsText(lstAllFiles[I]) then
      begin
        lstContent.LoadFromFile(lstAllFiles[I]);
        //scan line per line for given searchKeyWord
        for J := 0 to lstContent.Count - 1 do
        begin
          if AnsiContainsText(LowerCase(lstContent[J]),
            LowerCase(FSearchKeyWord)) then
          begin
            if FOnTextFound<>nil then
            begin
              FOnTextFound(Self,lstContent[j],lstAllFiles[i]);
            end;
            break;
          end;
          if FShouldStop then
            Break;
        end;

      end
      else
      if FileIsDoc(lstAllFiles[I]) then
      begin
        try
          wFilename := UTF8Decode(lstAllFiles[I]);
          oleServer := CreateOleObject(OfficeServerName);
          oleDoc := OleServer.Documents.Open(wFilename);

          wText := oleDoc.Range.Text;
          if AnsiContainsText(wText, FSearchKeyWord) then
          begin
            FOnTextFound(Self,wText,lstAllFiles[i]);
          end;

        OleDoc.Close;
        OleServer.Quit;
        except on e:Exception do
          if FOnError<> nil then
          FOnError(Self,e.Message,lstAllFiles[i]);
        end;
      end else
      if FileIsPdf(lstAllFiles[I]) then
      begin
          try
            pdfDoc := CreateOleObject(PdfServerName);
            wFilename := UTF8Decode(lstAllFiles[I]);
            b := pdfDoc.Open(wFilename);

            if b then
            for j := 0 to pdfDoc.GetNumPages - 1 do
            begin
              PDPage := pdfDoc.acquirePage(j);

              //Create highlight object with 2000 elements
              PDHili := CreateOleObject(PdfHighlightServerName);
              b := PDHili.Add(0, 4096);

             //Mark all text
             if b then
             begin
               PDTextS := PDPage.CreatePageHilite(PDHili);
               pCount := PDTextS.GetNumText;
               for k := 0 to pCount - 1 do
               begin
                 wText := PDTextS.GetText(k);
                  if AnsiContainsText(wText, FSearchKeyWord) then
                  begin
                    if FOnTextFound<>nil then
                    begin
                      FOnTextFound(Self,wText,lstAllFiles[i]);
                    end;
                    break;
                  end;
                  if FShouldStop then Break;
               end;
             end;
              if FShouldStop then Break;
            end

          except on E:Exception do
            if FOnError<> nil then
            FOnError(Self,e.Message,lstAllFiles[i]);
          end;
      end;

      if FShouldStop then
        Break;
    end;

  finally
    lstAllFiles.Free;
    lstContent.Free;
    CoUninitialize;
  end;

end;

constructor TFileSearchThread.Create(SearchFolder, SearchKeyWord: string);
begin
  FShouldStop := False;
  FSearchFolder := SearchFolder;
  FSearchKeyWord := SearchKeyWord;
  inherited Create(True);
end;

procedure TFileSearchThread.Cancel;
begin
  FShouldStop := True;
end;

end.
